package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.demo.model.UserEntity;
import com.example.demo.model.dto.ErrorDto;
import com.example.demo.model.dto.UserDto;
import com.example.demo.repository.UserRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;
    

    private static final Pattern EMAIL_PATTERN = Pattern.compile("^[A-Za-z0-9+_.-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}(?:\\.[0-9]{3})?$");
    private static final Pattern PASSWORD_PATTERN = Pattern.compile("^(?=.*[0-9])(?=.*[a-zA-Z]).{8,}$");

    public List<ErrorDto> saveUser(UserDto userDto) {
        List<ErrorDto> errorsList = new ArrayList<>();

        if (!isValidEmail(userDto.getEmail())) {
            errorsList.add(new ErrorDto(401, "Non è valido il formato dell'email"));
        }

        if (!isValidPassword(userDto.getPassword())) {
            errorsList.add(new ErrorDto(402, "Non è valido il formato della password"));
        }

        if (emailExists(userDto.getEmail())) {
            errorsList.add(new ErrorDto(400, "Email già esistente"));
        }

        // Se ci sono errori restituisci la lista
        if (!errorsList.isEmpty()) {
            return errorsList;
        }

        UserEntity userEntity = new UserEntity();
        userEntity.setFirstName(userDto.getFirstName());
        userEntity.setLastName(userDto.getLastName());
        userEntity.setEmail(userDto.getEmail());
        userEntity.setPassword(userDto.getPassword());

        userRepository.save(userEntity);

        // In caso di successo, la lista sarà vuota
        return errorsList;
    }


    public boolean emailExists(String email) {
        return userRepository.existsByEmail(email);
    }

    public boolean isValidEmail(String email) {
        return email != null && EMAIL_PATTERN.matcher(email).matches();
    }

    public boolean isValidPassword(String password) {
        return password != null && PASSWORD_PATTERN.matcher(password).matches();
    }
}
