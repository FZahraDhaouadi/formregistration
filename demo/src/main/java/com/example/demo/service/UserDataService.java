package com.example.demo.service;

import org.springframework.stereotype.Service;
import com.example.demo.model.dto.UserDto;

import java.util.Random;

@Service
public class UserDataService {

    private static final String[] NOME = {"Andrea", "Sara", "Alex", "Emily", "Antonio", "Anna", "David", "Laura"};
    private static final String[] COGNOME = {"Dossi", "DeAntoni", "Johnson", "Marrone", "Williams", "Baggio", "Garcia", "Miller"};
    private static final String[] DOMINIO = {"gmail.com", "email.com", "yahoo.com"};
    private static final String CARATTERI = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    private final Random random = new Random();

   public UserDto generateRandomUser() {
        UserDto userDto = new UserDto();
        userDto.setFirstName(getRandomElement(NOME));
        userDto.setLastName(getRandomElement(COGNOME));
        userDto.setEmail(generateRandomEmail(userDto.getFirstName(), userDto.getLastName()));
        userDto.setPassword(generateRandomPassword());
        return userDto;
    } 

    private String getRandomElement(String[] array) {
        return array[random.nextInt(array.length)];
    }

    private String generateRandomEmail(String firstName, String lastName) {
        String domain = getRandomElement(DOMINIO);
        return firstName.toLowerCase() + "." + lastName.toLowerCase() + "@" + domain;
    }

    private String generateRandomPassword() {
        StringBuilder password = new StringBuilder();

        password.append(CARATTERI.charAt(random.nextInt(CARATTERI.length() - 10) + 10)); // +10 per iniziare da 10
        
        for (int i = 1; i < 10; i++) {
            password.append(CARATTERI.charAt(random.nextInt(CARATTERI.length())));
        }
        
        return password.toString();
    }
}
