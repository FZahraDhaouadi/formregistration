package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.example.demo.model.dto.ErrorDto;
import com.example.demo.model.dto.UserDto;
import com.example.demo.service.UserDataService;
import com.example.demo.service.UserService;
import java.util.List;

@RestController
@RequestMapping("/api/register")
public class RegistrationController {

    @Autowired
    private UserService userService;
    
    @Autowired
    private UserDataService userDataService;
    
    @PostMapping
    public ResponseEntity<?> registerUser(@RequestBody UserDto userDto) {
        List<ErrorDto> errors = userService.saveUser(userDto);
        if (!errors.isEmpty()) {
            return ResponseEntity.badRequest().body(errors);
        }
        return ResponseEntity.ok().body("Registrazione avvenuta con successo");
    }


    @GetMapping("/complete")
    public ResponseEntity<UserDto> autocompleteFields() {
        UserDto randomUserDto = userDataService.generateRandomUser(); 
        return ResponseEntity.ok().body(randomUserDto);
    }
}
