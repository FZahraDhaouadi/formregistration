package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.example.demo.model.dto.UserDto;
import com.example.demo.service.UserService;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class demoAppRunner  implements CommandLineRunner{
     
    @Autowired
    UserService UserService;
    
    @Override
    public void run(String... args) throws Exception{

        log.info("run go");


        UserDto u1 = new UserDto("andrea" , "bass", "anna@mail.com", "prova123");
        UserDto u2 = new UserDto("gio" , "ste", "bho@mail.com", "prova123");
       /* u2.setFirstName("anna");getter e settr x costruct vuoto  */
        UserDto u3 = new UserDto("anna" , "masso", "masso@mail.com", "prova123");

        UserService.saveUser(u1);
        UserService.saveUser(u2);
        UserService.saveUser(u3);
      
        log.info("run end");

}
}