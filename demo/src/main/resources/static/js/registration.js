$(document).ready(function() {
    $('#registrationForm').submit(function(event) {
        event.preventDefault(); // Blocca il caricamento predefinito

        // Validazione dei campi
        var formData = {
            firstName: $('#firstName').val(),
            lastName: $('#lastName').val(),
            email: $('#email').val(),
            password: $('#password').val()
        };

        // Rimuove le classi di errore esistenti
        $('.error-field').removeClass('error-field');
        $('.error-message').hide();

        // Invia i dati al server
        $.ajax({
            type: 'POST',
            url: '/api/register',
            contentType: 'application/json',
            data: JSON.stringify(formData),
            success: function(response) {
                // Gestione successo
                $('#successMessage').text('Registrazione avvenuta con successo!').fadeIn().delay(3000).fadeOut();
            },
            error: function(xhr, status, error) {
                console.error('Si è verificato un errore durante la registrazione:', xhr.responseText);

                var responseJson;

                // Gestione degli errori
                if (xhr.status === 400) {
                    try {
                        responseJson = JSON.parse(xhr.responseText);
                        
                        if (Array.isArray(responseJson)) {
                            responseJson.forEach(function(error) {
                                var errorCode = error.code;
                                var errorMessage = error.message;

                                if (errorCode === 401 || errorCode === 400) {
                                    $('#emailError').text(errorMessage).show();
                                    $('#email').addClass('error-field');
                                } else if (errorCode === 402) {
                                    $('#passwordError').text(errorMessage).show();
                                    $('#password').addClass('error-field');
                                } else {
                                    console.error('Errore non gestito:', errorMessage);
                                }
                            });
                        }
                    } catch (e) {
                        console.error('Errore nel parsing della risposta JSON:', e);
                    }
                }
            }
        });
    });


    $('#autoComplete').click(function() {
        $.ajax({
            type: 'GET',
            url: '/api/register/complete',
            contentType: 'application/json',
            success: function(response) {

                $('#firstName').val(response.firstName);
                $('#lastName').val(response.lastName);
                $('#email').val(response.email);
                $('#password').val(response.password);
       
            },
            error: function(xhr, status, error) {
                console.error('Si è verificato un errore durante l\'autocompletamento:', xhr.responseText);
            }
        });
    });
});
